#!/bin/bash
# SPDX-License-Identifier: LGPL-2.1+
# ~~~
#   runtest.sh of dhcpcd
#   Description: a DHCP and DHCPv6 client. It's also an IPv4LL (aka ZeroConf) client.
#
#   Author: Susant Sahani <susant@redhat.com>
#   Copyright (c) 2018 Red Hat, Inc.
# ~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="dhcpcd"

DHCPCD_CI_DIR="/var/run/dhcpcd-ci"
DHCPCD_DUID_FILE="/etc/dhcpcd.duid"

RESOLVE_CONF="/etc/resolv.conf"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE

        rlRun "systemctl stop firewalld" 0,5
        rlRun "setenforce 0" 0,1

        rlFileBackup "$RESOLVE_CONF"
        rlFileBackup "$DHCPCD_DUID_FILE"
        rlRun "[ -e /sys/class/net/veth-test ] && ip link del veth-test" 0,1
	rlRun "TESTDIR=$(pwd)"

        rlLog "Create work dir ..."
        rlRun "mkdir -p $DHCPCD_CI_DIR"
        rlRun "cp *.conf $DHCPCD_CI_DIR"
    rlPhaseEnd

    rlPhaseStartTest
        rlLog "Starting dhcpcd tests ..."
        rlRun "/usr/bin/python3 ${TESTDIR}/dhcpcd-tests.py"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "[ -e /sys/class/net/veth-test ] && ip link del veth-test" 0,1

        rlFileRestore

	rlBundleLogs dhcpcd-ci ${DHCPCD_CI_DIR}/*log*
        rlLog "remove work dir"
        #rlRun "rm -rf $DHCPCD_CI_DIR"

        rlRun "setenforce 1" 0,1
        rlLog "dhcpcd tests done"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd

rlGetTestState
