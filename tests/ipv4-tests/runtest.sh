#!/bin/bash
# SPDX-License-Identifier: LGPL-2.1+
# ~~~
#   runtest.sh of dhcpcd
#   Description: a DHCP and DHCPv6 client. It's also an IPv4LL (aka ZeroConf) client.
#
#   Author: Susant Sahani <susant@redhat.com>
#   Copyright (c) 2018 Red Hat, Inc.
# ~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="dhcpcd"
DHCPCD_CI_DIR="/var/run/dhcpcd-ci"

SERVICE_UNITDIR="/var/run/systemd/system"
RESOLVE_CONF="/etc/resolv.conf"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "systemctl stop firewalld" 0,5
        rlRun "setenforce 0" 0,1
	rlRun "TESTDIR=$(pwd)"

        rlFileBackup --missing-ok "$RESOLVE_CONF"

        rlRun "[ -e /sys/class/net/veth-test ] && ip link del veth-test" 0,1

        rlLog "Create work dir ..."
        rlRun "mkdir -p $DHCPCD_CI_DIR"
        rlRun "cp *.conf $DHCPCD_CI_DIR"

        rlRun "cp tcpdumpd.service $SERVICE_UNITDIR"
        rlRun "cp dhcpcd-tests.py /usr/local/sbin/"

        rlRun "systemctl daemon-reload"
    rlPhaseEnd

    rlPhaseStartTest
        rlLog "Starting dhcpcd tests ..."
        rlRun "/usr/bin/python3 ${TESTDIR}/dhcpcd-tests.py"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "[ -e /sys/class/net/veth-test ] && ip link del veth-test" 0,1

        rlFileRestore

        rlLog "remove work dir"
	rlBundleLogs dhcpcd-ci ${DHCPCD_CI_DIR}/*log*
        rlRun "rm -rf $DHCPCD_CI_DIR"

        rlRun "rm $SERVICE_UNITDIR/tcpdumpd.service"
        rlRun "systemctl daemon-reload"

        rlRun "setenforce 1" 0,1
        rlLog "dhcpcd tests done"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd

rlGetTestState
